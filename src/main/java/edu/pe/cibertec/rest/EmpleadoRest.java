package edu.pe.cibertec.rest;


import edu.pe.cibertec.model.Empleado;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/api/empleados/")
public class EmpleadoRest {

    Empleado empleado;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public List<Empleado> getEmpleado() {
        return Empleado.listAll();
    }


    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Empleado obtenerEmpleado(@PathParam("id") Long id){
        Empleado lst = new Empleado(id);
        Empleado emp = obtenerEmpleado(id);
        emp.getId();
        emp.getNombre();
        emp.getSalario();
        emp.getFechaIngreso();
        return lst;
    }




}